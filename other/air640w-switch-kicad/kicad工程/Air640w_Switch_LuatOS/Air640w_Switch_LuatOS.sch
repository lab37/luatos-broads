EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Air640w_Switch_LuatOS-rescue:air640w-My_GSM_SIMCOM-Air640w_Switch_LuatOS-rescue U1
U 1 1 602C0022
P 1950 1450
F 0 "U1" H 1950 2315 50  0000 C CNN
F 1 "air640w" H 1950 2224 50  0000 C CNN
F 2 "My_RF_Modules:air640w" H 1950 1500 50  0001 C CNN
F 3 "" H 1950 1500 50  0001 C CNN
	1    1950 1450
	1    0    0    -1  
$EndComp
$Comp
L Air640w_Switch_LuatOS-rescue:GND-power-Air640w_Switch_LuatOS-rescue #PWR0101
U 1 1 602C261A
P 850 1050
F 0 "#PWR0101" H 850 800 50  0001 C CNN
F 1 "GND" V 855 922 50  0000 R CNN
F 2 "" H 850 1050 50  0001 C CNN
F 3 "" H 850 1050 50  0001 C CNN
	1    850  1050
	0    1    1    0   
$EndComp
NoConn ~ 850  1250
NoConn ~ 3050 1050
$Comp
L Air640w_Switch_LuatOS-rescue:GND-power-Air640w_Switch_LuatOS-rescue #PWR0102
U 1 1 602C2D5C
P 3050 1150
F 0 "#PWR0102" H 3050 900 50  0001 C CNN
F 1 "GND" V 3055 1022 50  0000 R CNN
F 2 "" H 3050 1150 50  0001 C CNN
F 3 "" H 3050 1150 50  0001 C CNN
	1    3050 1150
	0    -1   -1   0   
$EndComp
Text Label 3050 1450 0    50   ~ 0
PB7
Text Label 3050 1650 0    50   ~ 0
TX0
Text Label 3050 1750 0    50   ~ 0
RX0
$Comp
L Air640w_Switch_LuatOS-rescue:USB_B_Micro-Connector-Air640w_Switch_LuatOS-rescue J1
U 1 1 602C70AE
P 3850 1450
F 0 "J1" H 3907 1917 50  0000 C CNN
F 1 "USB_B_Micro" H 3907 1826 50  0000 C CNN
F 2 "Connector_USB:USB_Micro-B_Molex_47346-0001" H 4000 1400 50  0001 C CNN
F 3 "~" H 4000 1400 50  0001 C CNN
	1    3850 1450
	1    0    0    -1  
$EndComp
$Comp
L Air640w_Switch_LuatOS-rescue:CH330N-Interface_USB-Air640w_Switch_LuatOS-rescue U2
U 1 1 602C89C3
P 5050 1350
F 0 "U2" H 5050 1831 50  0000 C CNN
F 1 "CH330N" H 5050 1740 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 4900 2100 50  0001 C CNN
F 3 "http://www.wch.cn/downloads/file/240.html" H 4950 1550 50  0001 C CNN
	1    5050 1350
	1    0    0    -1  
$EndComp
Text Label 4150 1450 0    50   ~ 0
D+
Text Label 4150 1550 0    50   ~ 0
D-
NoConn ~ 4150 1650
$Comp
L Air640w_Switch_LuatOS-rescue:GND-power-Air640w_Switch_LuatOS-rescue #PWR0103
U 1 1 602C95AE
P 3850 1900
F 0 "#PWR0103" H 3850 1650 50  0001 C CNN
F 1 "GND" H 3855 1727 50  0000 C CNN
F 2 "" H 3850 1900 50  0001 C CNN
F 3 "" H 3850 1900 50  0001 C CNN
	1    3850 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 1850 3850 1850
Connection ~ 3850 1850
Wire Wire Line
	3850 1900 3850 1850
Text Label 4650 1250 2    50   ~ 0
3V3
Text Label 4650 1450 2    50   ~ 0
D+
Text Label 4650 1550 2    50   ~ 0
D-
Text Label 5450 1250 0    50   ~ 0
RX0
Text Label 5450 1350 0    50   ~ 0
TX0
$Comp
L Air640w_Switch_LuatOS-rescue:GND-power-Air640w_Switch_LuatOS-rescue #PWR0104
U 1 1 602CA671
P 5050 1750
F 0 "#PWR0104" H 5050 1500 50  0001 C CNN
F 1 "GND" H 5055 1577 50  0000 C CNN
F 2 "" H 5050 1750 50  0001 C CNN
F 3 "" H 5050 1750 50  0001 C CNN
	1    5050 1750
	1    0    0    -1  
$EndComp
Text Label 5050 1050 0    50   ~ 0
3V3
Wire Wire Line
	7450 1300 7800 1300
Wire Wire Line
	6450 1300 6350 1300
$Comp
L Air640w_Switch_LuatOS-rescue:+5V-power-Air640w_Switch_LuatOS-rescue #PWR0105
U 1 1 602CD337
P 6000 1300
F 0 "#PWR0105" H 6000 1150 50  0001 C CNN
F 1 "+5V" H 6015 1473 50  0000 C CNN
F 2 "" H 6000 1300 50  0001 C CNN
F 3 "" H 6000 1300 50  0001 C CNN
	1    6000 1300
	1    0    0    -1  
$EndComp
$Comp
L Air640w_Switch_LuatOS-rescue:C-Device-Air640w_Switch_LuatOS-rescue C1
U 1 1 602CDBE6
P 6000 1450
F 0 "C1" H 6115 1496 50  0000 L CNN
F 1 "10u" H 6115 1405 50  0000 L CNN
F 2 "lc_lib:0603_C" H 6038 1300 50  0001 C CNN
F 3 "~" H 6000 1450 50  0001 C CNN
	1    6000 1450
	1    0    0    -1  
$EndComp
Connection ~ 6000 1300
$Comp
L Air640w_Switch_LuatOS-rescue:C-Device-Air640w_Switch_LuatOS-rescue C2
U 1 1 602CE357
P 6350 1450
F 0 "C2" H 6465 1496 50  0000 L CNN
F 1 "0.1u" H 6465 1405 50  0000 L CNN
F 2 "lc_lib:0603_C" H 6388 1300 50  0001 C CNN
F 3 "~" H 6350 1450 50  0001 C CNN
	1    6350 1450
	1    0    0    -1  
$EndComp
Connection ~ 6350 1300
Wire Wire Line
	6350 1300 6000 1300
$Comp
L Air640w_Switch_LuatOS-rescue:C-Device-Air640w_Switch_LuatOS-rescue C3
U 1 1 602CEC63
P 7450 1450
F 0 "C3" H 7565 1496 50  0000 L CNN
F 1 "10u" H 7565 1405 50  0000 L CNN
F 2 "lc_lib:0603_C" H 7488 1300 50  0001 C CNN
F 3 "~" H 7450 1450 50  0001 C CNN
	1    7450 1450
	1    0    0    -1  
$EndComp
$Comp
L Air640w_Switch_LuatOS-rescue:C-Device-Air640w_Switch_LuatOS-rescue C4
U 1 1 602CF336
P 7800 1450
F 0 "C4" H 7915 1496 50  0000 L CNN
F 1 "0.1u" H 7915 1405 50  0000 L CNN
F 2 "lc_lib:0603_C" H 7838 1300 50  0001 C CNN
F 3 "~" H 7800 1450 50  0001 C CNN
	1    7800 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 1600 6350 1600
Wire Wire Line
	7450 1600 7800 1600
$Comp
L Air640w_Switch_LuatOS-rescue:GND-power-Air640w_Switch_LuatOS-rescue #PWR0106
U 1 1 602D069F
P 6950 1700
F 0 "#PWR0106" H 6950 1450 50  0001 C CNN
F 1 "GND" H 6955 1527 50  0000 C CNN
F 2 "" H 6950 1700 50  0001 C CNN
F 3 "" H 6950 1700 50  0001 C CNN
	1    6950 1700
	1    0    0    -1  
$EndComp
Connection ~ 7450 1600
$Comp
L Air640w_Switch_LuatOS-rescue:GND-power-Air640w_Switch_LuatOS-rescue #PWR0107
U 1 1 602D1256
P 7100 1600
F 0 "#PWR0107" H 7100 1350 50  0001 C CNN
F 1 "GND" H 7105 1427 50  0000 C CNN
F 2 "" H 7100 1600 50  0001 C CNN
F 3 "" H 7100 1600 50  0001 C CNN
	1    7100 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 1600 7450 1600
Wire Wire Line
	7800 1300 7800 1200
Connection ~ 7800 1300
Text Label 7800 1200 0    50   ~ 0
3V3
$Comp
L Air640w_Switch_LuatOS-rescue:C-Device-Air640w_Switch_LuatOS-rescue C5
U 1 1 602D9EEE
P 1750 4200
F 0 "C5" H 1635 4154 50  0000 R CNN
F 1 "0.1u" H 1650 4250 50  0000 R CNN
F 2 "lc_lib:0603_C" H 1788 4050 50  0001 C CNN
F 3 "~" H 1750 4200 50  0001 C CNN
	1    1750 4200
	-1   0    0    1   
$EndComp
$Comp
L Air640w_Switch_LuatOS-rescue:R-Device-Air640w_Switch_LuatOS-rescue R1
U 1 1 602DB018
P 1750 3800
F 0 "R1" H 1820 3846 50  0000 L CNN
F 1 "1K" H 1820 3755 50  0000 L CNN
F 2 "lc_lib:0603_R" V 1680 3800 50  0001 C CNN
F 3 "~" H 1750 3800 50  0001 C CNN
	1    1750 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1750 4050 1750 4000
Text Label 1750 4050 0    50   ~ 0
RESET
Text Label 1750 3650 0    50   ~ 0
3V3
Wire Wire Line
	1750 4350 1000 4350
Wire Wire Line
	1000 4350 1000 4050
$Comp
L Air640w_Switch_LuatOS-rescue:GND-power-Air640w_Switch_LuatOS-rescue #PWR0108
U 1 1 602E0189
P 1000 4350
F 0 "#PWR0108" H 1000 4100 50  0001 C CNN
F 1 "GND" H 1005 4177 50  0000 C CNN
F 2 "" H 1000 4350 50  0001 C CNN
F 3 "" H 1000 4350 50  0001 C CNN
	1    1000 4350
	1    0    0    -1  
$EndComp
Connection ~ 1000 4350
$Comp
L Air640w_Switch_LuatOS-rescue:C-Device-Air640w_Switch_LuatOS-rescue C6
U 1 1 602E36D3
P 3200 4150
F 0 "C6" H 3085 4104 50  0000 R CNN
F 1 "0.1u" H 3100 4200 50  0000 R CNN
F 2 "lc_lib:0603_C" H 3238 4000 50  0001 C CNN
F 3 "~" H 3200 4150 50  0001 C CNN
	1    3200 4150
	-1   0    0    1   
$EndComp
$Comp
L Air640w_Switch_LuatOS-rescue:R-Device-Air640w_Switch_LuatOS-rescue R2
U 1 1 602E36DD
P 3200 3750
F 0 "R2" H 3270 3796 50  0000 L CNN
F 1 "1K" H 3270 3705 50  0000 L CNN
F 2 "lc_lib:0603_R" V 3130 3750 50  0001 C CNN
F 3 "~" H 3200 3750 50  0001 C CNN
	1    3200 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 4000 3200 3900
Text Label 3200 4000 0    50   ~ 0
PB7
Text Label 3200 3600 0    50   ~ 0
3V3
Connection ~ 3200 4000
Wire Wire Line
	3200 4300 2450 4300
$Comp
L Air640w_Switch_LuatOS-rescue:GND-power-Air640w_Switch_LuatOS-rescue #PWR0109
U 1 1 602E36EE
P 2450 4300
F 0 "#PWR0109" H 2450 4050 50  0001 C CNN
F 1 "GND" H 2455 4127 50  0000 C CNN
F 2 "" H 2450 4300 50  0001 C CNN
F 3 "" H 2450 4300 50  0001 C CNN
	1    2450 4300
	1    0    0    -1  
$EndComp
Connection ~ 2450 4300
Connection ~ 7450 1300
$Comp
L Air640w_Switch_LuatOS-rescue:SOT-223_AMS1117-3.3-lc_LDO-Air640w_Switch_LuatOS-rescue 0.1u1
U 1 1 602CB9DE
P 6750 1700
F 0 "0.1u1" H 6950 2455 50  0000 C CNN
F 1 "SOT-223_AMS1117-3.3" H 6500 2350 50  0001 L BNN
F 2 "lc_lib:SOT-223" H 6450 1499 50  0001 L BNN
F 3 "http://www.szlcsc.com/product/details_6652.html" H 6450 1599 50  0001 L BNN
F 4 "低压差线性稳压(LDO)" H 6750 1700 50  0001 C CNN "description"
F 5 "供应商链接" H 6450 1399 50  0001 L BNN "ComponentLink1Description"
F 6 "SOT-223" H 6450 1299 50  0001 L BNN "Package"
F 7 "LC" H 6450 1199 50  0001 L BNN "Supplier"
F 8 "C6186" H 6450 1099 50  0001 L BNN "SuppliersPartNumber"
F 9 "GND" H 6950 2364 50  0000 C CNN "user"
F 10 "" H 6450 899 50  0001 L BNN "Notepad"
F 11 "AMS1117-3.3" H 6950 2273 50  0000 C CNN "Comment"
	1    6750 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 1600 6350 1600
Connection ~ 7100 1600
Connection ~ 6350 1600
Wire Wire Line
	1000 4050 1200 4050
NoConn ~ 850  1350
NoConn ~ 850  1650
NoConn ~ 1750 2550
NoConn ~ 2150 2550
NoConn ~ 3050 1550
NoConn ~ 3050 1350
Text GLabel 5450 1550 2    50   Input ~ 0
RESET
Text GLabel 3050 1250 2    50   Input ~ 0
RESET
$Comp
L Air640w_Switch_LuatOS-rescue:+5V-power-Air640w_Switch_LuatOS-rescue #PWR0110
U 1 1 602E18EC
P 4150 1250
F 0 "#PWR0110" H 4150 1100 50  0001 C CNN
F 1 "+5V" H 4165 1423 50  0000 C CNN
F 2 "" H 4150 1250 50  0001 C CNN
F 3 "" H 4150 1250 50  0001 C CNN
	1    4150 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1200 4050 1200 3900
Wire Wire Line
	1600 3900 1600 4000
Wire Wire Line
	1600 4000 1750 4000
Connection ~ 1750 4000
Wire Wire Line
	1750 4000 1750 3950
Wire Wire Line
	1200 4050 1400 4050
Connection ~ 1200 4050
Text Label 850  1750 2    50   ~ 0
PWM+
Text Label 1850 2550 3    50   ~ 0
PWM-
$Comp
L Air640w_Switch_LuatOS-rescue:Conn_01x04_Female-Connector-Air640w_Switch_LuatOS-rescue J2
U 1 1 6032093C
P 1350 3000
F 0 "J2" H 1378 2976 50  0000 L CNN
F 1 "Conn_01x04_Female" H 1378 2885 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 1350 3000 50  0001 C CNN
F 3 "~" H 1350 3000 50  0001 C CNN
	1    1350 3000
	1    0    0    -1  
$EndComp
Text Label 1150 2900 2    50   ~ 0
PWM+
Text Label 1150 3100 2    50   ~ 0
PWM-
$Comp
L Air640w_Switch_LuatOS-rescue:GND-power-Air640w_Switch_LuatOS-rescue #PWR0111
U 1 1 60323323
P 750 3050
F 0 "#PWR0111" H 750 2800 50  0001 C CNN
F 1 "GND" H 755 2877 50  0000 C CNN
F 2 "" H 750 3050 50  0001 C CNN
F 3 "" H 750 3050 50  0001 C CNN
	1    750  3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 3000 900  3000
Wire Wire Line
	750  3000 750  3050
Wire Wire Line
	1150 3200 900  3200
Wire Wire Line
	900  3200 900  3000
Connection ~ 900  3000
Wire Wire Line
	900  3000 750  3000
$Comp
L Air640w_Switch_LuatOS-rescue:Conn_01x02_Female-Connector-Air640w_Switch_LuatOS-rescue J5
U 1 1 603260FA
P 2450 3100
F 0 "J5" H 2342 2775 50  0000 C CNN
F 1 " " H 2342 2866 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2450 3100 50  0001 C CNN
F 3 "~" H 2450 3100 50  0001 C CNN
	1    2450 3100
	-1   0    0    1   
$EndComp
Connection ~ 2450 4000
Wire Wire Line
	2450 4000 2450 4300
Wire Wire Line
	2450 3850 2450 4000
Wire Wire Line
	2850 4000 2450 4000
Wire Wire Line
	2650 3850 2450 3850
Wire Wire Line
	3050 3850 3050 4000
Wire Wire Line
	3050 4000 3200 4000
Wire Wire Line
	2650 3100 2750 3100
Wire Wire Line
	2750 3100 2750 3150
$Comp
L Air640w_Switch_LuatOS-rescue:GND-power-Air640w_Switch_LuatOS-rescue #PWR0112
U 1 1 603320AC
P 2750 3150
F 0 "#PWR0112" H 2750 2900 50  0001 C CNN
F 1 "GND" H 2755 2977 50  0000 C CNN
F 2 "" H 2750 3150 50  0001 C CNN
F 3 "" H 2750 3150 50  0001 C CNN
	1    2750 3150
	1    0    0    -1  
$EndComp
Text Label 3050 950  0    50   ~ 0
3V3
$Comp
L Air640w_Switch_LuatOS-rescue:R-Device-Air640w_Switch_LuatOS-rescue R3
U 1 1 60332FCB
P 2800 3000
F 0 "R3" V 2880 2960 50  0000 L CNN
F 1 "1K" V 2800 2950 50  0000 L CNN
F 2 "lc_lib:0603_R" V 2730 3000 50  0001 C CNN
F 3 "~" H 2800 3000 50  0001 C CNN
	1    2800 3000
	0    1    1    0   
$EndComp
Text Label 2950 3000 0    50   ~ 0
3V3
NoConn ~ 850  950 
NoConn ~ 850  1150
$Comp
L Air640w_Switch_LuatOS-rescue:iot_Power-rescue_SW_3*6*3.5mm-My_SWITCH-iot_Power-cache-Air640w_Switch_LuatOS-rescue SW2
U 1 1 60411F60
P 2850 3850
F 0 "SW2" H 2850 4085 50  0000 C CNN
F 1 " " H 2850 3994 50  0000 C CNN
F 2 "iot_Power:3x6x3.5_SW_side" H 2750 3700 50  0001 C CNN
F 3 "" H 2875 3850 50  0001 C CNN
	1    2850 3850
	1    0    0    -1  
$EndComp
$Comp
L Air640w_Switch_LuatOS-rescue:iot_Power-rescue_SW_3*6*3.5mm-My_SWITCH-iot_Power-cache-Air640w_Switch_LuatOS-rescue SW1
U 1 1 60412B63
P 1400 3900
F 0 "SW1" H 1400 4135 50  0000 C CNN
F 1 " " H 1400 4044 50  0000 C CNN
F 2 "iot_Power:3x6x3.5_SW_side" H 1300 3750 50  0001 C CNN
F 3 "" H 1425 3900 50  0001 C CNN
	1    1400 3900
	1    0    0    -1  
$EndComp
$Comp
L Air640w_Switch_LuatOS-rescue:LED-Device-Air640w_Switch_LuatOS-rescue LED1
U 1 1 604163CE
P 4350 2550
F 0 "LED1" V 4389 2432 50  0000 R CNN
F 1 "LED" V 4298 2432 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 4350 2550 50  0001 C CNN
F 3 "~" H 4350 2550 50  0001 C CNN
	1    4350 2550
	0    -1   -1   0   
$EndComp
Text Label 850  1450 2    50   ~ 0
LED1
Text Label 850  1550 2    50   ~ 0
LED2
$Comp
L Air640w_Switch_LuatOS-rescue:R-Device-Air640w_Switch_LuatOS-rescue R5
U 1 1 60418E63
P 4350 2250
F 0 "R5" V 4430 2210 50  0000 L CNN
F 1 "1K" V 4350 2200 50  0000 L CNN
F 2 "lc_lib:0603_R" V 4280 2250 50  0001 C CNN
F 3 "~" H 4350 2250 50  0001 C CNN
	1    4350 2250
	-1   0    0    1   
$EndComp
Text Label 4350 2100 0    50   ~ 0
LED1
$Comp
L Air640w_Switch_LuatOS-rescue:GND-power-Air640w_Switch_LuatOS-rescue #PWR0113
U 1 1 604195A9
P 4350 2700
F 0 "#PWR0113" H 4350 2450 50  0001 C CNN
F 1 "GND" H 4355 2527 50  0000 C CNN
F 2 "" H 4350 2700 50  0001 C CNN
F 3 "" H 4350 2700 50  0001 C CNN
	1    4350 2700
	1    0    0    -1  
$EndComp
$Comp
L Air640w_Switch_LuatOS-rescue:LED-Device-Air640w_Switch_LuatOS-rescue LED2
U 1 1 6041C161
P 4800 2550
F 0 "LED2" V 4839 2432 50  0000 R CNN
F 1 "LED" V 4748 2432 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 4800 2550 50  0001 C CNN
F 3 "~" H 4800 2550 50  0001 C CNN
	1    4800 2550
	0    -1   -1   0   
$EndComp
$Comp
L Air640w_Switch_LuatOS-rescue:R-Device-Air640w_Switch_LuatOS-rescue R6
U 1 1 6041C167
P 4800 2250
F 0 "R6" V 4880 2210 50  0000 L CNN
F 1 "1K" V 4800 2200 50  0000 L CNN
F 2 "lc_lib:0603_R" V 4730 2250 50  0001 C CNN
F 3 "~" H 4800 2250 50  0001 C CNN
	1    4800 2250
	-1   0    0    1   
$EndComp
Text Label 4800 2100 0    50   ~ 0
LED2
$Comp
L Air640w_Switch_LuatOS-rescue:GND-power-Air640w_Switch_LuatOS-rescue #PWR0114
U 1 1 6041C16E
P 4800 2700
F 0 "#PWR0114" H 4800 2450 50  0001 C CNN
F 1 "GND" H 4805 2527 50  0000 C CNN
F 2 "" H 4800 2700 50  0001 C CNN
F 3 "" H 4800 2700 50  0001 C CNN
	1    4800 2700
	1    0    0    -1  
$EndComp
NoConn ~ 2050 2550
NoConn ~ 1950 2550
$EndSCHEMATC
